﻿using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float speed = 6f;        //Speed of the player

    Vector3 movement;               // Vector to store the dreiction of the player's movement
    Animator anim;                  // Reference to animator component
    Rigidbody playerRigidbody;      // Reference to the player's rigidbody
    int floorMask;                  // A layer mask so that a ray can be cast just at gameobjects on the floor layer
    float camRayLength = 100f;      // Length of the ray from camera into the scene

    /**
     * Similar to start function but is called regardless of whether or not the script is enabled
     */
    void Awake()
    {
        floorMask = LayerMask.GetMask("Floor");
        anim = GetComponent<Animator>();
        playerRigidbody = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        float h = Input.GetAxisRaw("Horizontal");
        float v = Input.GetAxisRaw("Vertical");

        Move(h, v);
        Turning();
        Animating(h, v);
    }

    void Move(float h, float v)
    {
        movement.Set(h, 0f, v);
        movement = movement.normalized * speed * Time.deltaTime;

        playerRigidbody.MovePosition(transform.position + movement);
    }

    void Turning()
    {
        Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);

        RaycastHit floorHit;
        if (Physics.Raycast(camRay, out floorHit, camRayLength, floorMask))
        {
            Vector3 playerToMouse = floorHit.point - transform.position;
            playerToMouse.y = 0f;

            Quaternion newRotation = Quaternion.LookRotation(playerToMouse);
            playerRigidbody.MoveRotation(newRotation);
        } 
    }

    void Animating (float h, float v)
    {
        bool walking = h != 0f || v != 0f;
        anim.SetBool("IsWalking", walking);
    }
}
