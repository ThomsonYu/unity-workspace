﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Clock : MonoBehaviour {

    public Transform hoursTransfrom, minutesTransform, secondsTrandsform;
    public bool continuous;

    const float
        degreesPerHour = 30f,
        degreesPerMinute = 6f,
        degreesPerSecond = 6f;

    private void Update()
    {
        if (continuous)
        {
            UpdateContinuous();
        }
        else
        {
            UpdateDiscrete();
        }
    }

    private void UpdateContinuous()
    {
        TimeSpan time = DateTime.Now.TimeOfDay;
        hoursTransfrom.localRotation = Quaternion.Euler(0f, (float)time.TotalHours * degreesPerHour, 0f);
        minutesTransform.localRotation = Quaternion.Euler(0f, (float)time.TotalMinutes * degreesPerMinute, 0f);
        secondsTrandsform.localRotation = Quaternion.Euler(0f, (float)time.TotalSeconds * degreesPerMinute, 0f);
    }

    private void UpdateDiscrete()
    {
        DateTime time = DateTime.Now;
        hoursTransfrom.localRotation = Quaternion.Euler(0f, time.Hour * degreesPerHour, 0f);
        minutesTransform.localRotation = Quaternion.Euler(0f, time.Minute * degreesPerMinute, 0f);
        secondsTrandsform.localRotation = Quaternion.Euler(0f, time.Second * degreesPerMinute, 0f);
    }
}
